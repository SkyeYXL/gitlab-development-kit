#!/usr/bin/env bash

set -euo pipefail

error() {
  echo
  echo "ERROR: ${1}" >&2
  exit 1
}

# Ensure we're a supported shell before progressing any further..
#
case "${SHELL}" in
*/zsh|*/bash)
  true
  ;;
*)
  error "Unsupported shell. Only bash and zsh supported currently." >&2
  ;;
esac

CURRENT_ASDF_DIR="${ASDF_DIR:-${HOME}/.asdf}"
CURRENT_ASDF_DATA_DIR="${ASDF_DATA_DIR:-$CURRENT_ASDF_DIR}"

export PATH="${CURRENT_ASDF_DIR}/bin:${CURRENT_ASDF_DATA_DIR}/shims:${PATH}"

ASDF_VERSION="v0.8.0"
BUNDLER_VERSION=$(grep -A1 'BUNDLED WITH' Gemfile.lock | tail -n1 | tr -d ' ')

asdf_install() {
  if [[ ! -d "${CURRENT_ASDF_DIR}" ]]; then
    git clone https://github.com/asdf-vm/asdf.git "${CURRENT_ASDF_DIR}" --branch ${ASDF_VERSION}

    asdf_add_initializer "${HOME}/.bashrc" "asdf.sh"
    asdf_add_initializer "${HOME}/.zshrc" "asdf.sh"

    return 0
  fi

  return 0
}

asdf_add_initializer() {
  [[ -f "${1}" ]] && echo -e "\n# Added by GDK bootstrap\nsource ${CURRENT_ASDF_DIR}/${2}" >> "${1}"
}

asdf_install_plugins() {
  cut -d ' ' -f 1 .tool-versions | grep -v '^#' | while IFS= read -r plugin
  do
    if ! asdf plugin list | grep -xq "^${plugin}$"; then
      asdf plugin add "${plugin}" || true
    fi
  done

  # Install Node.js' OpenPGP key
  if [[ ! -f "${HOME}/.gnupg/asdf-nodejs.gpg" ]]; then
    bash -c "${CURRENT_ASDF_DATA_DIR}/plugins/nodejs/bin/import-release-team-keyring" > /dev/null 2>&1
  fi

  return 0
}

asdf_install_packages() {
  # Install all packages specified in .tool-versions
  ruby_configure_opts=$(ruby_configure_opts)
  bash -c "MAKELEVEL=0 ${ruby_configure_opts} asdf install"

  return $?
}

asdf_reshim() {
  asdf reshim
}

gdk_install_gem() {
  if ! echo_if_unsuccessful asdf exec gem install bundler -v "= ${BUNDLER_VERSION}"; then
    return 1
  fi

  if ! echo_if_unsuccessful asdf exec gem install gitlab-development-kit; then
    return 1
  fi

  return 0
}

gdk_prepare_directory() {
  gdk trust . > /dev/null 2>&1

  [[ ! -f ".gdk-install-root"  ]] && echo "${PWD}" > .gdk-install-root

  return 0
}

configure_ruby_bundler() {
  local current_postgres_version
  current_postgres_version=$(asdf current postgres | awk '{ print $2 }')

  bundle config build.pg "--with-pg-config=${CURRENT_ASDF_DIR}/installs/postgres/${current_postgres_version}/bin/pg_config"
  bundle config build.thin --with-cflags="-Wno-error=implicit-function-declaration"
}

ensure_not_root() {
  if [[ ${EUID} -eq 0 ]]; then
    return 1
  fi

  return 0
}

ensure_supported_platform() {
  if [[ "${OSTYPE}" == "darwin"* ]]; then
    return 0
  elif [[ "${OSTYPE}" == "linux-gnu"* ]]; then
    os_id=$(awk -F= '$1=="ID" { print $2 ;}' /etc/os-release)

    if [[ "$os_id" == "ubuntu" || "$os_id" == "debian" ]]; then
      return 0
    fi
  fi

  return 1
}

setup_platform() {
  if [[ "${OSTYPE}" == "darwin"* ]]; then
    if ! setup_platform_macos; then
      return 1
    fi
  elif [[ "${OSTYPE}" == "linux-gnu"* ]]; then
    os_id=$(awk -F= '$1=="ID" { print $2 ;}' /etc/os-release)

    if [[ "${os_id}" == "ubuntu" ]]; then
      if ! setup_platform_ubuntu; then
        return 1
      fi
    elif [[ "${os_id}" == "debian" ]]; then
      if ! setup_platform_debian; then
        return 1
      fi
    fi
  fi
}

setup_platform_ubuntu() {
  if ! echo_if_unsuccessful sudo apt-get update; then
    return 1
  fi

  # shellcheck disable=SC2046
  if ! sudo apt-get install -y $(sed -e 's/#.*//' packages.txt); then
    return 1
  fi

  return 0
}

setup_platform_debian() {
  if ! echo_if_unsuccessful sudo apt-get update; then
    return 1
  fi

  # shellcheck disable=SC2046
  if ! sudo apt-get install -y $(sed -e 's/#.*//' packages_debian.txt); then
    return 1
  fi

  return 0
}

setup_platform_macos() {
  local shell_file ruby_configure_opts

  if [ -z "$(command -v brew)" ]; then
    echo "INFO: Installing Homebrew.."
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
  fi

  if ! brew bundle; then
    return 1
  fi

  if ! echo_if_unsuccessful brew link pkg-config; then
    return 1
  fi

  if ! echo_if_unsuccessful brew pin libffi icu4c readline re2; then
    return 1
  fi

  case $SHELL in
  */zsh)
    shell_file="${HOME}/.zshrc"
    ;;
  *)
    shell_file="${HOME}/.bashrc"
    ;;
  esac

  icu4c_pkgconfig_path="export PKG_CONFIG_PATH=\"/usr/local/opt/icu4c/lib/pkgconfig:\${PKG_CONFIG_PATH}\""
  if ! grep -Fxq "${icu4c_pkgconfig_path}" "${shell_file}"; then
    echo -e "\n# Added by GDK bootstrap\n${icu4c_pkgconfig_path}" >> "${shell_file}"
  fi

  ruby_configure_opts="export $(ruby_configure_opts)"
  if ! grep -Fxq "${ruby_configure_opts}" "${shell_file}"; then
    echo -e "\n# Added by GDK bootstrap\n${ruby_configure_opts}" >> "${shell_file}"
  fi

  if [[ ! -d "/Applications/Google Chrome.app" ]]; then
    if ! brew list --cask google-chrome > /dev/null 2>&1; then
      if ! brew cask install google-chrome; then
        return 1
      fi
    fi
  fi
}

ruby_configure_opts() {
  if [[ "${OSTYPE}" == "darwin"* ]]; then
    brew_openssl_dir=$(brew --prefix openssl)
    brew_readline_dir=$(brew --prefix readline)

    echo "RUBY_CONFIGURE_OPTS=\"--with-openssl-dir=${brew_openssl_dir} --with-readline-dir=${brew_readline_dir}\""
  fi

  return 0
}

ensure_required_software_available() {
  if [ -z "$(command -v sudo)" ]; then
    echo "ERROR: sudo command not found!" >&2
    return 1
  fi

  return 0
}

echo_if_unsuccessful() {
  output="$("${@}" 2>&1)"

  # shellcheck disable=SC2181
  if [[ $? -ne 0 ]] ; then
    echo "${output}" >&2
    return 1
  fi
}

###############################################################################

if ! ensure_supported_platform; then
  echo
  echo "ERROR: Unsupported platform. Only macOS, Ubuntu, and Debian supported." >&2
  echo "INFO: Please visit https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/doc/advanced.md to bootstrap manually." >&2
  return 1
fi

if ! ensure_not_root; then
  error "Running as root is not supported."
fi

if ! ensure_required_software_available; then
  error "Some required software is not installed, please install." >&2
fi

echo "INFO: Setting up platform.."
if ! setup_platform; then
  error "Failed to install set up platform." >&2
fi

echo "INFO: Installing asdf.."
if ! asdf_install; then
  error "asdf install failed to install." >&2
fi

echo "INFO: Installing asdf plugins.."
if ! asdf_install_plugins; then
  error "Failed to install some asdf plugins." >&2
fi

echo "INFO: Installing asdf packages.."
if ! asdf_install_packages; then
  error "Failed to install some asdf packages." >&2
fi

echo "INFO: Reshimming asdf.."
if ! asdf_reshim; then
  error "Failed to reshim asdf." >&2
fi

echo "INFO: Installing gitlab-development-kit Ruby gem.."
if ! gdk_install_gem; then
  error "Failed to install gitlab-development-kit Ruby gem." >&2
fi

echo "INFO: Preparing GDK directory.."
if ! gdk_prepare_directory; then
  error "Failed to prepare GDK directory." >&2
fi

echo "INFO: Configuring Ruby Bundler.."
if ! configure_ruby_bundler; then
  error "Failed to configure Ruby Bundler." >&2
fi

echo
echo "INFO: Bootstrap successful!"
echo "INFO: To make sure GDK commands are available, run:"
echo
echo "source \"${CURRENT_ASDF_DIR}/asdf.sh\""
echo
